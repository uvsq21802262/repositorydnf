
public enum Component {

	AND ,//'^' = 0
	OR ,//'v' = 1
	OP ,//'(' = 2
	CP ,//')' = 3
	X ,//'X' = 4
	Y  ;//'Y' = 5
	
	public char calculate(Component symbole) {
		  switch(symbole) {
		  case AND: return '^';
		  case OR: return 'v';
		  case OP: return '(';
		  case CP: return ')';
		  case X: return 'X';
		  case Y: return 'Y';
		  }
		  throw new AssertionError("Opération inconnue : " + this);
	  }
}

