import java.util.ArrayList;

public class DNFGenerator {
	
	StringBuilder s;
	private int Min;
	private int Max;
	
	
	Component component = Component.AND;
	
	public StringBuilder generate()
	{
		int random = (int)(Min + (Math.random() * (Max - Min)));
		s = new StringBuilder(random);
		Component[] alist = {Component.AND,Component.OR,Component.OP,Component.CP,Component.X,Component.Y};
		int cpt = 0;
		while(cpt < random)
		{
			s.append(component.calculate(alist[(int) (0 + (Math.random() * (5 - 0)))]));
			cpt++;
		}
		
		return s;
	}


	public void setMin(int min) {
		Min = min;
	}

	public void setMax(int max) {
		Max = max;
	}
	
	
	
	
	
	
}
